<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conciertos extends Model
{
    // use HasFactory;


    protected $fillable = ['nombre', 'numero_espectadores', 'fecha', 'rentabilidad', 'id_promotor', 'id_recinto'];

    public static function create(array $attributes = [])
    {
        $model = static::query()->create($attributes);
        if (isset($attributes['grupos']))
            $model->grupos()->attach($attributes['grupos']);
        if (isset($attributes['medios']))
            $model->medios()->attach($attributes['medios']);
        $model->rentabilidad = $model->obtener_rentabilidad();

        return $model;
    }

   
    public function obtener_rentabilidad()
    {
        $beneficios = $this->recinto->precio_entrada * $this->numero_espectadores * 0.9;
        $gastos = $this->recinto->coste_alquiler + $this->grupos->sum('cache');
        return $beneficios - $gastos;
    }

    public function promotor()
    {
        return $this->belongsTo(Promotores::class, 'id_promotor');
    }

    public function recinto()
    {
        return $this->belongsTo(Recintos::class, 'id_recinto');
    }

    public function grupos()
    {
        return $this->belongsToMany(Grupos::class, 'grupos_conciertos', 'id_concierto', 'id_grupo');
    }

    public function medios()
    {
        return $this->belongsToMany(Medios::class, 'grupos_medios', 'id_concierto', 'id_medio');
    }
}
