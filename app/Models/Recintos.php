<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recintos extends Model
{
    use HasFactory;

    public function conciertos()
    {
        return $this->hasMany(Conciertos::class, 'id_recinto');
    }
}
