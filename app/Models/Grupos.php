<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
    //use HasFactory;

    public function conciertos()
    {
        return $this->belongsToMany(Conciertos::class, 'grupos_conciertos', 'id_grupo', 'id_concierto');
    }
}
