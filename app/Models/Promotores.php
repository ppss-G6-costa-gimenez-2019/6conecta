<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotores extends Model
{
   // use HasFactory;

    public function conciertos()
    {
        return $this->hasMany(Conciertos::class, 'id_promotor');
    }
}
