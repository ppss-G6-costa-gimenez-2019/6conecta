<?php

namespace App\Http\Controllers;

use App\Models\Recintos;
use Illuminate\Http\Request;

class RecintosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recintos  $recintos
     * @return \Illuminate\Http\Response
     */
    public function show(Recintos $recintos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recintos  $recintos
     * @return \Illuminate\Http\Response
     */
    public function edit(Recintos $recintos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recintos  $recintos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recintos $recintos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recintos  $recintos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recintos $recintos)
    {
        //
    }
}
