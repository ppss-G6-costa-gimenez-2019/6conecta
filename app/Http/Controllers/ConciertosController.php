<?php

namespace App\Http\Controllers;

use App\Models\Conciertos;
use Illuminate\Http\Request;
use Exception;
use Mail;

class ConciertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //se podria crear un StoreConciertoRequest para separar la validacion del controller
        //validar datos del post
        $validated = $request->validate([
            'nombre' => 'required',
            'fecha' => 'required',
            'id_recinto' => 'required',
            'grupos' => 'required',
            'numero_espectadores' => 'required',
            'id_promotor' => 'required',
            'medios' => 'required',
        ]);

        try {
            //se ha movido la creacion de las relaciones dentro de la funcion create del modelo
            $concierto = Conciertos::create($request->all());
            //el correo no esta configurado, por eso lo comento
            //$this->enviar_correo_concierto_creado($concierto);
        } catch (Exception $e) {
            return back()->withErrors(['error', 'Error al crear el concierto'.$e->getMessage()]);
        }
    }

    public function enviar_correo_concierto_creado($concierto)
    {
        $data = array('nombre_concierto' => $concierto->nombre, 'rentabilidad' => $concierto->rentabilidad);

        Mail::send(['text' => 'mail'], $data, function ($message) {
            $message->to('6conecta@gmail.com', '6conecta')->subject('Concierto creado');
            $message->from('6conecta@gmail.com', '6conecta');
        });
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conciertos  $conciertos
     * @return \Illuminate\Http\Response
     */
    public function show(Conciertos $conciertos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conciertos  $conciertos
     * @return \Illuminate\Http\Response
     */
    public function edit(Conciertos $conciertos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conciertos  $conciertos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conciertos $conciertos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conciertos  $conciertos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conciertos $conciertos)
    {
        //
    }
}
