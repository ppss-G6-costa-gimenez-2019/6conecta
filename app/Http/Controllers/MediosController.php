<?php

namespace App\Http\Controllers;

use App\Models\Medios;
use Illuminate\Http\Request;

class MediosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Medios  $medios
     * @return \Illuminate\Http\Response
     */
    public function show(Medios $medios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Medios  $medios
     * @return \Illuminate\Http\Response
     */
    public function edit(Medios $medios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medios  $medios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medios $medios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Medios  $medios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medios $medios)
    {
        //
    }
}
